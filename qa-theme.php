<?php

class qa_html_theme extends qa_html_theme_base
{
	protected $ranking_block_layout = true;
	protected $theme = 'respostas-ocultas';

	public function nav_user_search()
	{
		if (!qa_is_logged_in()) {
			if (isset($this->content['navigation']['user']['login']) && !QA_FINAL_EXTERNAL_USERS) {
				$login = $this->content['navigation']['user']['login'];
				$emailOnly = qa_opt('allow_login_email_only');
				$inputType = $emailOnly ? 'email' : 'text';
				$this->output(
					'<form class="qam-login-form" action="'.$login['url'].'" method="post">',
						'<input type="' . $inputType . '" class="qam-login-text" name="emailhandle" dir="auto" placeholder="'.trim(qa_lang_html($emailOnly ? 'users/email_label' : 'users/email_handle_label'), ':').'"/>',
						'<input type="password" class="qam-login-text" name="password" dir="auto" placeholder="'.trim(qa_lang_html('users/password_label'), ':').'"/>',
						'<div class="qam-rememberbox"><input type="checkbox" name="remember" id="qam-rememberme" value="1"/>',
						'<label for="qam-rememberme" class="qam-remember">'.qa_lang_html('users/remember').'</label></div>',
						'<input type="hidden" name="code" value="'.qa_html(qa_get_form_security_code('login')).'"/>',
						'<input type="submit" value="Faça Login" class="qa-form-tall-button qa-form-tall-button-login" name="dologin"/>',
					'</form>'
				);

				unset($this->content['navigation']['user']['login']);
			}
		}

		qa_html_theme_base::nav_user_search();
	}

	public function logged_in()
	{
		require_once QA_INCLUDE_DIR . 'app/format.php';

		if (qa_is_logged_in()) // output user avatar to login bar
			$this->output(
				'<div class="qa-logged-in-avatar">',
				QA_FINAL_EXTERNAL_USERS
				? qa_get_external_avatar_html(qa_get_logged_in_userid(), 24, true)
				: qa_get_user_avatar_html(qa_get_logged_in_flags(), qa_get_logged_in_email(), qa_get_logged_in_handle(),
					qa_get_logged_in_user_field('avatarblobid'), qa_get_logged_in_user_field('avatarwidth'), qa_get_logged_in_user_field('avatarheight'),
					24, true),
				'</div>'
			);

		qa_html_theme_base::logged_in();

		if (qa_is_logged_in()) { // adds points count after logged in username
			$userpoints = qa_get_logged_in_points();

			$pointshtml = ($userpoints == 1)
				? qa_lang_html_sub('main/1_point', '1', '1')
				: qa_lang_html_sub('main/x_points', qa_html(qa_format_number($userpoints)));

			$this->output(
				'<span class="qa-logged-in-points">',
				'('.$pointshtml.')',
				'</span>'
			);
		}
	}

	public function body_header()
	{
		$this->output('<div class="qam-login-bar">');
		$this->output('<div class="qam-login-group">');
		$this->nav_user_search();
		$this->output('</div></div>');
		$this->nav_main_sub();
	}	

	public function head_css()
	{
		$this->output('<link rel="stylesheet" href="' . $this->rooturl . $this->css_name() . '"/>');
		$this->output('<script src="https://kit.fontawesome.com/b5f666874b.js" crossorigin="anonymous"></script>');

		if (isset($this->content['css_src'])) {
			foreach ($this->content['css_src'] as $css_src) {
				$this->output('<link rel="stylesheet" href="' . $css_src . '"/>');
			}
		}

		if (!empty($this->content['notices'])) {
			$this->output(
				'<style>',
				'.qa-body-js-on .qa-notice {display:none;}',
				'</style>'
			);
		}
	}

	public function nav_main_sub()
	{
		$this->nav('main');
		$this->nav('sub');
	}



	// allows modification of custom element shown inside header after logo
	public function header_custom()
	{
		if (isset($this->content['body_header'])) {
			$this->output('<div class="header-banner">');
			$this->output_raw($this->content['body_header']);
			$this->output('</div>');
		}
	}

	// removes user navigation and search from header and replaces with custom header content. Also opens new <div>s
	public function header()
	{

		$this->header_clear();
		$this->logo();
		$this->header_custom();
		$this->output('</div> <!-- END qa-header -->', '');


		$this->output('<div class="qa-main-shadow">', '');
		$this->output('<div class="qa-main-wrapper">', '');
	}

	// removes sidebar for user profile pages
	public function sidepanel()
	{
		if ($this->template != 'user')
			qa_html_theme_base::sidepanel();
	}

	// prevent display of regular footer content (see body_suffix()) and replace with closing new <div>s
	public function footer()
	{
		$this->output('</div> <!-- END main-wrapper -->');
		$this->output('</div> <!-- END main-shadow -->');
	}

	// add RSS feed icon after the page title
	public function favorite()
	{
		parent::favorite();

		$feed = @$this->content['feed'];

		if (!empty($feed)) {
			$this->output('<a href="'.$feed['url'].'" title="'.@$feed['label'].'"><img src="'.$this->rooturl.'images/rss.jpg" alt="" width="16" height="16" class="qa-rss-icon"/></a>');
		}
	}

	// add view count to question list
	public function q_item_stats($q_item)
	{
		$this->output('<div class="qa-q-item-stats">');

		$this->voting($q_item);
		$this->a_count($q_item);
		qa_html_theme_base::view_count($q_item);

		$this->output('</div>');
	}

	// prevent display of view count in the usual place
	public function view_count($q_item)
	{
		if ($this->template == 'question')
			qa_html_theme_base::view_count($q_item);
	}

	// to replace standard Q2A footer
	public function body_suffix()
	{
		$this->output('<div class="qa-footer-bottom-group">');
		qa_html_theme_base::footer();
		$this->output('</div> <!-- END footer-bottom-group -->', '');
	}

	public function before_attribution()
	{

		$this->output(
			'<div class="qa-attribution">',
			'Copyright (c) 2022 Respostas Ocultas - ',
			'</div>'
		);
	}

	public function attribution()
	{
		$this->output(
			'<div class="qa-attribution">',
			'Tema criado com <i id="created-by-icon-heart" class="fa-solid fa-heart" style="color: red;"></i> por <a href="http://xh6liiypqffzwnu5734ucwps37tn2g6npthvugz3gdoqpikujju525yd.onion/index.php/user/Slug">Slug</a>',
			'</div>',
		);

		$this->before_attribution();
	}
}
